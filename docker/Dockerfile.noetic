FROM quay.io/andrijdavid/cartographer_ros:noetic

ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NOWARNINGS yes
ENV NVIDIA_VISIBLE_DEVICES ${NVIDIA_VISIBLE_DEVICES:-all}
ENV NVIDIA_DRIVER_CAPABILITIES ${NVIDIA_DRIVER_CAPABILITIES:+$NVIDIA_DRIVER_CAPABILITIES,}graphics
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION v14.16.0

RUN apt update -qqy && apt install -qqy wget mesa-utils libgl1-mesa-glx x11-apps tmux  ros-noetic-vision-opencv \
      ros-noetic-tf2-geometry-msgs ros-noetic-tf2-sensor-msgs ros-noetic-tf2-ros \
      openssh-server ros-noetic-laser-assembler ros-noetic-uuid-msgs ros-noetic-unique-id libglfw3-dev libglew-dev libeigen3-dev \
      libjsoncpp-dev python3-pip libtclap-dev ros-noetic-rviz ros-noetic-pcl-ros nvidia-settings libsdl2-dev cargo curl

RUN wget https://raw.githubusercontent.com/milq/milq/master/scripts/bash/install-opencv.sh && \
    sed -i  "s/OPENCV_VERSION='4.2.0'/OPENCV_VERSION='3.4.13'/g" install-opencv.sh && \
    sed -i  "s/OPENCV_CONTRIB='NO'/OPENCV_CONTRIB='YES'/g" install-opencv.sh && \
    bash install-opencv.sh && \
    rm install-opencv.sh && \
    wget https://download.ensenso.com/s/ensensosdk/download?files=ensenso-sdk-3.0.311-x64.deb -O ensenso.deb && \
    dpkg -i ensenso.deb && \
    rm ensenso.deb

RUN apt install -qqy ros-noetic-pcl-ros && pip3 install pyyaml

RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y

RUN mkdir -p ${NVM_DIR} && curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
RUN . ${NVM_DIR}/nvm.sh && \
    nvm install ${NODE_VERSION} && \
    nvm alias default $NODE_VERSION && \
    nvm use default

ENV NODE_PATH $NVM_DIR/versions/node/$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/versions/node/$NODE_VERSION/bin:$PATH

RUN npm install -g yarn

RUN git clone https://github.com/cartographer-project/point_cloud_viewer /point_cloud_viewer && \
    cd /point_cloud_viewer && \
    git submodule update --init --recursive && \
    cargo build --release && \
    cd /point_cloud_viewer/sdl_viewer/ && \
    cargo build --release && \
    cd /point_cloud_viewer/octree_web_viewer/client && \
    yarn install && \
    yarn run build && \
    cd /point_cloud_viewer/octree_web_viewer/ && \
    cargo build --release

ENV PATH /point_cloud_viewer/target/release:${PATH}

RUN mkdir /davidSDK && \
    cd /davidSDK && \
    git clone https://gitlab.com/InstitutMaupertuis/davidSDK.git src && \
    mkdir build && \
    cd build && \
    cmake ../src && \
    make && \
    make install && \
    rm -rf /davidSDK

RUN cp /etc/ssh/sshd_config /etc/ssh/sshd_config_bak && \
    sed -i "s/^.*X11Forwarding.*$/X11Forwarding yes/" /etc/ssh/sshd_config && \
    sed -i "s/^.*X11UseLocalhost.*$/X11UseLocalhost no/" /etc/ssh/sshd_config && \
    grep "^X11UseLocalhost" /etc/ssh/sshd_config || echo "X11UseLocalhost no" >> /etc/ssh/sshd_config && \
    ssh-keygen -A  && \
    sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config && \
    echo 'root:root' | chpasswd

RUN rm -rf /var/lib/apt/lists/*

EXPOSE 22

##  xhost +local:
## docker run --gpus all -it --rm --name cart -p 2150:22 -e DISPLAY=$DISPLAY -v="/tmp/.X11-unix:/tmp/.X11-unix:ro"  -v ${PWD}:/root/mapping-tools quay.io/andrijdavid/cartographer:noetic