#!/bin/bash
source ../devel/setup.bash
base_path="/mnt/b6ef98c1-c7b0-4b69-87d0-2165d664c748/bags/";

bag_filename="/root/mapping-tools/debug/offline.bag"
#bag_filenames="${base_path}landmarkdebug/map_gen_0.bag";

roslaunch ouster_slam os_offline_slam.launch \
                             replay:=true \
                             rviz:=true \
                             bag_filename:="${bag_filename}"