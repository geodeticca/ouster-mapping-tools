#!/bin/bash
source ../devel/setup.bash
base_path="/mnt/b6ef98c1-c7b0-4b69-87d0-2165d664c748/bags/";

bag_filenames="/root/mapping-tools/debug/2021-02-03-13-40-07.bag"
config_path="/root/mapping-tools/debug/os-992038000083.local.json"

#
#bag_filenames="/home/mapping-tools/debug/Y.bag"
#config_path="/home/mapping-tools/debug/os-992038000083.local.json"

#record_filepath="/home/andrijdavid/Project/geo/08_SLAM_test_20210203-20210205T072609Z-001/08_SLAM_test_20210203/";

roslaunch ouster_slam os_slam.launch \
                             udp_hostname:=127.0.0.1 \
                             replay:=true \
                             bag_filename:="${bag_filenames}"\
                             metadata:=${config_path}