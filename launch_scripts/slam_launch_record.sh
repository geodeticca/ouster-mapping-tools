#!/bin/bash
source ../../devel/setup.bash

base_path="/mnt/b6ef98c1-c7b0-4b69-87d0-2165d664c748/bags/";
bag_filenames="${base_path}kostolany/os1_bag__2020-05-04-19-50-47_0.bag ${base_path}kostolany/os1_bag__2020-05-04-19-51-54_1.bag"
config_path="${base_path}kostolany/caconfig.json" #own path
record_filepath="${base_path}kostolany/";

roslaunch ouster_slam os_slam_record.launch \
                             udp_hostname:=127.0.0.1 \
                             replay:=true \
                             metadata:=${config_path}\
                             bag_filename:="${bag_filenames}"\
                             record:=true\
                             record_filepath:=${record_filepath}
