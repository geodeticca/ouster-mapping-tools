#!/bin/bash
source ../devel/setup.bash

bag_filenames="/home/mapping-tools/debug/2021-02-03-13-40-07.bag"
config_path="/home/mapping-tools/debug/os-992038000083.local.json"

roslaunch ouster_slam ouster.launch \
                             udp_hostname:=127.0.0.1 \
                             replay:=true \
                             bag_filename:=${bag_filenames}\
                             metadata:=${config_path}\
                             record:=false\
                             viz:=true