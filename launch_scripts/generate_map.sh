#!/bin/bash
source ../devel/setup.bash

base_path="/mnt/b6ef98c1-c7b0-4b69-87d0-2165d664c748/bags/";

bag_filenames="/root/mapping-tools/debug/offline.bag"
pbstream_filename="/root/mapping-tools/debug/offline.bag.pbstream"

#bag_filenames="${base_path}landmarkdebug/map_gen_0.bag"
#pbstream_filename="${base_path}landmarkdebug/map_gen_0.bag.pbstream"


roslaunch ouster_slam assets_writer_ros_map.launch \
   bag_filenames:="${bag_filenames}" \
   pose_graph_filename:=${pbstream_filename}\
   conf_filename:="assets_writer_ouster.lua"