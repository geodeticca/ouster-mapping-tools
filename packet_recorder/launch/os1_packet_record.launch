<launch>

  <arg name="os1_hostname" default="" doc="hostname or IP in dotted decimal form of the sensor"/>
  <arg name="os1_udp_dest" default="" doc="hostname or IP where the sensor will send data packets"/>
  <arg name="os1_lidar_port" default="0" doc="port to which the sensor should send lidar data"/>
  <arg name="os1_imu_port" default="0" doc="port to which the sensor should send imu data"/>
  <arg name="replay" default="false" doc="do not connect to a sensor; expect /os1_node/{lidar,imu}_packets from replay"/>
  <arg name="lidar_mode" default="" doc="resolution and rate: either 512x10, 512x20, 1024x10, 1024x20, or 2048x10"/>
  <arg name="timestamp_mode" default="" doc="method used to timestamp measurements: TIME_FROM_INTERNAL_OSC, TIME_FROM_SYNC_PULSE_IN, TIME_FROM_PTP_1588"/>
  <arg name="metadata" default="" doc="override default metadata file for replays"/>
  <arg name="tf_prefix" default="" doc="namespace for tf transforms"/>
  <arg name="arg bag_filename" default="" doc="bag for usage "/>
  <arg name="arg record" default="false" doc="record data"/>
  <arg name="arg record_filepath" default="~" doc="file path for the bag to record data"/>

  <arg name="multipurpose_io_mode" default="" doc="Configure the mode of the MULTIPURPOSE_IO pin. Valid modes are OFF , INPUT_NMEA_UART , OUTPUT_FROM_INTERNAL_OSC , OUTPUT_FROM_SYNC_PULSE_IN , OUTPUT_FROM_PTP_1588 , or OUTPUT_FROM_ENCODER_ANGLE ."/>
  <arg name="nmea_baud_rate" default="" doc="Set BAUD_9600 (default) or BAUD_115200 for the expected baud rate the sensor is attempting to decode for NMEA UART input $GPRMC messages."/>
  <arg name="nmea_ignore_valid_char" default="" doc="Set 0 if NMEA UART input $GPRMC messages should be ignored if valid charater is not set, and 1 if messages should be used for time syncing regardless of the valid character"/>
  <arg name="nmea_in_polarity" default="" doc="Set the polarity of NMEA UART input $GPRMC messages. Use ACTIVE_HIGH if UART is active high, idle low, and start bit is after a falling edge."/>
  <arg name="nmea_leap_seconds" default="" doc="Set an integer number of leap seconds that will be added to the UDP timesstamp when calculating seconds since 00:00:00 Thursday, 1 January 1970. For Unix Epoch time, this should be set to 0 ."/>
  <arg name="sync_pulse_in_polarity" default="" doc="Set the polarity of SYNC_PULSE_OUT output, if sensor is set as the master sensor used for time synchronization."/>

  <arg name="viz" default="false" doc="whether to run a simple visualizer"/>
  <arg name="image" default="false" doc="publish range/intensity/noise image topic"/>

  <node pkg="ouster_ros" name="os1_node" type="os1_node" output="screen" required="true">
    <param name="~/lidar_mode" type="string" value="$(arg lidar_mode)"/>
    <param name="~/timestamp_mode" type="string" value="$(arg timestamp_mode)"/>
    <param name="~/replay" value="$(arg replay)"/>
    <param name="~/os1_hostname" value="$(arg os1_hostname)"/>
    <param name="~/os1_udp_dest" value="$(arg os1_udp_dest)"/>
    <param name="~/os1_lidar_port" value="$(arg os1_lidar_port)"/>
    <param name="~/os1_imu_port" value="$(arg os1_imu_port)"/>
    <param name="~/metadata" value="$(arg metadata)"/>

    <param name="~/multipurpose_io_mode" value="$(arg multipurpose_io_mode)"/>
    <param name="~/nmea_baud_rate" value="$(arg nmea_baud_rate)"/>
    <param name="~/nmea_ignore_valid_char" value="$(arg nmea_ignore_valid_char)"/>
    <param name="~/nmea_in_polarity" value="$(arg nmea_in_polarity)"/>
    <param name="~/nmea_leap_seconds" value="$(arg nmea_leap_seconds)"/>
    <param name="~/sync_pulse_in_polarity" value="$(arg sync_pulse_in_polarity)"/>
  </node>

  <node pkg="ouster_ros" type="os1_cloud_node" name="os1_cloud_node" output="screen" required="true">
    <remap from="~/os1_config" to="/os1_node/os1_config"/>
    <remap from="~/lidar_packets" to="/os1_node/lidar_packets"/>
    <remap from="~/imu_packets" to="/os1_node/imu_packets"/>
    <param name="~/tf_prefix" value="$(arg tf_prefix)"/>
  </node>

  <node if="$(arg replay)" name="playbag" pkg="rosbag" type="play"
        args="--clock $(arg bag_filename)">
  </node>

  <node if="$(arg record)" name="record" pkg="rosbag" type="record"
        args="record /os1_node/imu_packets /os1_node/lidar_packets /tf_static  --split --size 1024 --chunksize=2048  -o  $(arg record_filepath)os1_packets">
  </node>

  <node if="$(arg viz)" pkg="ouster_ros" name="viz_node" type="viz_node" output="screen" required="true">
    <remap from="~/os1_config" to="/os1_node/os1_config"/>
    <remap from="~/points" to="/os1_cloud_node/points"/>
  </node>

  <node if="$(arg image)" pkg="ouster_ros" name="img_node" type="img_node" output="screen" required="true">
    <remap from="~/os1_config" to="/os1_node/os1_config"/>
    <remap from="~/points" to="/os1_cloud_node/points"/>
  </node>

</launch>
