#!/bin/bash
source ../../devel/setup.bash

bag_filenames="${base_path}5_min_static/2020-05-27-10-15-21.bag"
config_path="${base_path}5_min_static/os1-991937000688.local.json" #own path

base_path="/mnt/b6ef98c1-c7b0-4b69-87d0-2165d664c748/bags/";
bag_filenames="${base_path}kostolany/source/os1_bag__2020-05-04-19-50-47_0.bag ${base_path}kostolany/source/os1_bag__2020-05-04-19-51-54_1.bag"
config_path="${base_path}kostolany/source/config.json" #own path


RECORDFILEPATH="${base_path}debug/";
#imu

roslaunch packet_recorder os1_cloud_record.launch\
                             udp_hostname:=127.0.0.1\
                             timestamp_mode:="TIME_FROM_INTERNAL_OSC"\
                             replay:=true \
                             viz:=false\
                             bag_filename:="${bag_filenames}"\
                             metadata:=${config_path}\
                             record:=true \
                             record_filepath:=${RECORDFILEPATH}