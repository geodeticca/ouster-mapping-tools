#!/usr/bin/env python3

import rospy
from laser_assembler.srv import AssembleScans2
from sensor_msgs.msg import PointCloud2

rospy.init_node("map_assembler")
rospy.wait_for_service("assemble_scans2")
assemble_scans_srv = rospy.ServiceProxy('assemble_scans2', AssembleScans2)
pub = rospy.Publisher("/assembled_pointcloud", PointCloud2, queue_size=1)

r = rospy.Rate(1)
last_time = rospy.Time(0, 0)

while (not rospy.is_shutdown()):
    try:
        time_now = rospy.Time.now()
        resp = assemble_scans_srv(last_time, time_now)
        last_time = time_now
        pub.publish(resp.cloud)

    except rospy.ServiceException as e:
        print("Service call failed: ", e)

    r.sleep()
