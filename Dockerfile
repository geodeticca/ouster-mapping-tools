FROM quay.io/andrijdavid/cartographer:noetic

SHELL ["/bin/bash", "-c"]

RUN apt update -qq && apt install -y ros-noetic-gazebo-ros ros-noetic-gazebo-dev ros-noetic-imu-tools \
ros-noetic-mapviz ros-noetic-mapviz-plugins ros-noetic-tile-map ros-noetic-multires-image \
ros-noetic-fake-localization libignition-msgs libignition-msgs-dev libprotobuf-dev \
protobuf-compiler libprotoc-dev libignition-math6-dev ros-noetic-joy

RUN mkdir -p /root/catkin_ws/src /input /output

ADD . /root/catkin_ws/src

WORKDIR /root/catkin_ws/
RUN mkdir -p /root/.gazebo/models && cp -r /root/catkin_ws/src/car_demo/models/* /root/.gazebo/models/

RUN /ros_entrypoint.sh catkin_make -DCMAKE_BUILD_TYPE=Release -DBUILD_VIZ=ON
RUN /ros_entrypoint.sh pip3 install PyYAML

EXPOSE 5433
ENTRYPOINT ["/root/catkin_ws/src/scripts/entrypoint.sh"]