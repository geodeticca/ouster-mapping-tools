import rosbag
from itertools import chain

bags = ['output_imu.bag', '../debug/SLAM_ROAD/os_bag__2021-03-03-13-38-09_0.bag']
fbags = [rosbag.Bag(b).__iter__() for b in bags]
all_bag = chain(*fbags)

with rosbag.Bag('merged.bag', 'w') as bag:
    for topic, msg, t in all_bag:
        bag.write(topic, msg, t)
