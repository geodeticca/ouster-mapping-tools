import rosbag
import argparse
import sys

def main(args):
    parser = argparse.ArgumentParser(description='Print bag header .')
    parser.add_argument('bagfile', nargs=1, help='input bag file')
    args = parser.parse_args()
    bagfile = args.bagfile[0]
    last_t = []
    topics = []
    for topic, msg, t in rosbag.Bag(bagfile).read_messages():
        # This also replaces tf timestamps under the assumption
        # that all transforms in the message share the same timestamp
        last_t.append(t)
        topics.append(topic)
    print(msg)
    if (last_t == sorted(last_t)):
        print("Sorted")
    else:
        print("Not sorted")

if __name__ == "__main__":
    main(sys.argv[1:])
