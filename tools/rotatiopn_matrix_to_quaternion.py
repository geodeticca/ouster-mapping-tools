from scipy.spatial.transform import Rotation as R

r = R.from_matrix([[-0.632142, 0.027192, -0.774376],

                   [0.0492197, 0.998775, -0.00510751],

                   [0.773288, -0.0413432, -0.632706]])

print("Quaternion: ", r.as_quat())
print("Euler zyx: ", r.as_euler("zyx"))
