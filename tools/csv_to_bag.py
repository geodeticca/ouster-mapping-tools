import pandas as pd
import rospy
import rosbag
from sensor_msgs.msg import Imu, NavSatFix, Temperature, MagneticField
from std_msgs.msg import Header

df = pd.read_csv('../debug/SLAM_ROAD/RawSensors.csv')
df["Timestamp"] = df.apply(lambda row: float(row['Unix Time']) + (float(row['Microseconds']) * 1e6), axis=1)
df["Timestamp"] = df["Timestamp"].astype('float64')
df.sort_values(by='Timestamp', axis=0, ascending=True, inplace=True, ignore_index=True)
df.drop_duplicates(subset='Timestamp', keep="last", inplace=True)

gps_df = pd.read_csv('../debug/SLAM_ROAD/PostProcessed.csv')
gps_df["Timestamp"] = gps_df.apply(lambda row: float(row['Unix Time']) + (float(row['Microseconds']) * 1e6), axis=1)
gps_df["Timestamp"] = gps_df["Timestamp"].astype('float64')
gps_df.sort_values(by='Timestamp', axis=0, ascending=True, inplace=True, ignore_index=True)
gps_df.drop_duplicates(subset='Timestamp', keep="last", inplace=True)
# gps_df.set_index("Timestamp", inplace=True)

# assert len(df) == len(gps_df), f"The length of IMU {len(df)} and GPS {len(gps_df)} are not the same"
max_nanosec = 1614775166669408033
max_sec = max_nanosec / 1e9
with rosbag.Bag('output_imu.bag', 'w') as bag:
    # index, row in df.iterrows()
    # for row in range(df.shape[0]):
    for index, row in df.iterrows():
        # t = float(f"{df['Unix Time'][row]}{df['Microseconds'][row]}")
        #
        timestamp = rospy.Time.from_sec(row["Timestamp"])

        if float(row["Timestamp"]) > max_sec:
            break

        imu_msg = Imu()
        tmp_msg = Temperature()
        mag_msg = MagneticField()
        tmp_msg.variance = 0

        header = Header()
        header.stamp = timestamp
        header.frame_id = 'ins_imu'

        # tmp_msg.temperature = df['IMU Temperature (deg C)'][row]

        tmp_msg.temperature = row['IMU Temperature (deg C)']

        tmp_msg.header = header

        # Populate the data elements for Mag
        mag_msg.header = header

        mag_msg.magnetic_field.x = row['Magnetometer X (mG)'] * 1e-7  # Convert to Tesla
        mag_msg.magnetic_field.y = row['Magnetometer Y (mG)'] * 1e-7
        mag_msg.magnetic_field.z = row['Magnetometer Z (mG)'] * 1e-7

        # Populate the data elements for IMU
        imu_msg.header = header

        imu_msg.linear_acceleration.x = row['Accelerometer X (m/s/s)']
        imu_msg.linear_acceleration.y = row['Accelerometer Y (m/s/s)']
        imu_msg.linear_acceleration.z = row['Accelerometer Z (m/s/s)']

        imu_msg.linear_acceleration_covariance = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

        imu_msg.angular_velocity.x = row['Gyroscope X (degrees/s)'] * 0.017453  # Convert to rad/sec
        imu_msg.angular_velocity.y = row['Gyroscope Y (degrees/s)'] * 0.017453
        imu_msg.angular_velocity.z = row['Gyroscope Z (degrees/s)'] * 0.017453
        imu_msg.angular_velocity_covariance = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

        bag.write("/imu/raw", imu_msg, timestamp)
        bag.write("/temperature", tmp_msg, timestamp)
        bag.write("/mag", mag_msg, timestamp)

    last_time = None
    for index, row in gps_df.iterrows():
        if float(row["Timestamp"]) > max_sec:
            break

        gps_msg = NavSatFix()
        timestamp = rospy.Time.from_sec(row["Timestamp"])

        header = Header()
        header.stamp = timestamp
        header.frame_id = 'gps_fix'

        gps_msg.header = header

        gps_msg.latitude = row["Latitude"]
        gps_msg.longitude = row["Longitude"]
        gps_msg.altitude = row["Height"]

        # Populate the data elements for GPS
        bag.write("/fix", gps_msg, timestamp)