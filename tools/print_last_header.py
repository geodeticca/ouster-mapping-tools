import rosbag
import argparse
import sys


def main(args):
    parser = argparse.ArgumentParser(description='Print bag header .')
    parser.add_argument('bagfile', nargs=1, help='input bag file')
    args = parser.parse_args()
    bagfile = args.bagfile[0]
    last_h = None
    for topic, msg, t in rosbag.Bag(bagfile).read_messages():
        # This also replaces tf timestamps under the assumption
        # that all transforms in the message share the same timestamp
        if topic == "/os_node/lidar_packets":
            last_h = t
    print(last_h)


if __name__ == "__main__":
    main(sys.argv[1:])
