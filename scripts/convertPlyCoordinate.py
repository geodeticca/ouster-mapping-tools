import argparse
import pandas as pd
from pathlib import Path
from pyntcloud import PyntCloud

"""
Generate JTSK ply ot normal cordinate ply.
"""
parser = argparse.ArgumentParser()
parser.add_argument("--ply", help="Ply text file")
parser.add_argument("--output", help="Output file")
args = parser.parse_args()

if __name__ == "__main__":
    # X = -Y  and Y=-X for JTSK system
    # df = pd.read_csv(args.ply, names=['x', 'y', 'z'], sep=" ")
    # X = -1 * df["y"]
    # Y = -1 * df["x"]
    # df['x'] = X
    # df["y"] = Y
    cloud = PyntCloud.from_file(args.ply)
    print(cloud.points.describe())
    # df = cloud.points
    X = -1 * cloud.points["y"]
    Y = -1 * cloud.points["x"]
    cloud.points['x'] = X
    cloud.points["y"] = Y
    path = Path(args.output)
    path.parent.mkdir(parents=True, exist_ok=True)
    cloud.to_file(str(path))
    # df.to_csv(str(path) + '.csv', sep=" ", index=None)
