#!/bin/bash

xhost +local:
docker run --gpus all -it -e DISPLAY=$DISPLAY -v="/tmp/.X11-unix:/tmp/.X11-unix:ro" \
                             ouster-mapping-tools \
                             roslaunch "$@"