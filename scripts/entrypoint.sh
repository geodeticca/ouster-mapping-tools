#!/bin/bash

set -o errexit

# Setup ROs
source "/opt/ros/${ROS_DISTRO}/setup.bash"
source "/opt/cartographer_ros/setup.bash"
source "/root/catkin_ws/devel/setup.bash"

#rostopic bw /img_node/ambient_image & rostopic bw /img_node/intensity_image & rostopic bw /img_node/range_image

exec "$@"
