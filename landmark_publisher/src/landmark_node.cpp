/**
 * @file
 * @brief
 *
 * Publishes ~/
 */

#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include <vector>
#include <iostream>
#include <fstream>

#include <pcl/conversions.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/sample_consensus/sac_model_circle3d.h>
#include <pcl/sample_consensus/sac_model_sphere.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl_ros/point_cloud.h>

/*#include <ouster/os1.h>
#include <ouster/os1_packet.h>
#include <ouster/os1_util.h>*/

#include "ouster/build.h"
#include "ouster/client.h"
#include "ouster/lidar_scan.h"
#include "ouster/types.h"

#include <ouster_ros/OSConfigSrv.h>
#include <ouster_ros/ros.h>

#include <ros/ros.h>
#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/Image.h>

#include <unique_id/unique_id.h>
#include <boost/uuid/random_generator.hpp>

#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <image_transport/image_transport.h>
#include <ros/console.h>
#include <cartographer_ros_msgs/LandmarkEntry.h>
#include <cartographer_ros_msgs/LandmarkList.h>
#include <signal.h>

using namespace Eigen;
using PacketMsg = ouster_ros::PacketMsg;
using Cloud = ouster_ros::Cloud;
using Point = ouster_ros::Point;
namespace sensor = ouster::sensor;

// Global variable used to track the hieght and width initializaed by main
sensor::sensor_info info;

double areaSumOfAllChildrenOf(const std::vector<std::vector<cv::Point>> contours,const std::vector<cv::Vec4i> hierarchy, int i, cv::RotatedRect *rotRect) {
    double sumOfHoles = 0;
    int idx = i;

    cv::Vec4i currentChild = hierarchy[idx];

    if (currentChild[2] != -1) {
        idx = currentChild[2];
        currentChild = hierarchy[idx];

        sumOfHoles = sumOfHoles + contourArea(contours[idx]);
        if (contours[idx].size() > 5) {
            *rotRect = fitEllipse(contours[idx]);
        }
    }

    return sumOfHoles;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr  pointsAtIndex(std::vector<cv::Point> coords, const Cloud ptCloud) {
    pcl::PointCloud<pcl::PointXYZ>::Ptr contourPtCloud (new pcl::PointCloud<pcl::PointXYZ> ());
    extern sensor::sensor_info info;
    uint32_t H = info.format.pixels_per_column;
    uint32_t W = info.format.columns_per_frame;
    const auto px_offset = info.format.pixel_shift_by_row;

    for(cv::Point point : coords) {
//        const auto px_offset = ouster::OS1::get_px_offset(W);
        const size_t vv = (point.x + px_offset[point.y]) % W;
        const size_t index = vv * H + point.y;
        contourPtCloud->push_back(pcl::PointXYZ(ptCloud.points[index].x, ptCloud.points[index].y, ptCloud.points[index].z));
    }

    return contourPtCloud;
}

std::vector<pcl::PointXYZ> ptCloudIndicesOfLandMarks (const cv::Mat image, const float binaryThreshhold, const Cloud ptCloud) {
    std::vector<pcl::PointXYZ> landMarkCoordinates;

    double min_im, max_im;


    extern sensor::sensor_info info;
    uint32_t H = info.format.pixels_per_column;
    uint32_t W = info.format.columns_per_frame;
    const auto px_offset = info.format.pixel_shift_by_row;

    cv::minMaxLoc(image, &min_im, &max_im);

    cv::Mat im_norm(image);
    if (min_im == max_im) {
        PCL_INFO("Cannot normalize intensity image...\n");
    } else {

//        namedWindow("Targets", cv::WINDOW_AUTOSIZE);
//        imshow("Targets", image);
//        cv::waitKey(4);

        im_norm = (im_norm - min_im) / (max_im - min_im);

        cv::Mat threshIm;
        threshIm = image > binaryThreshhold;
//
//        namedWindow("Targets2", cv::WINDOW_AUTOSIZE);
//        imshow("Targets2", threshIm);
//        cv::waitKey(4);

        cv::Mat dst = cv::Mat::zeros(64, 2048, CV_8UC3);
        rotate(im_norm, im_norm, cv::ROTATE_90_CLOCKWISE);
        //TODO is everything kosher

        std::vector<std::vector<cv::Point> > contours;
        std::vector<cv::Vec4i> hierarchy;

        std::vector<std::vector<cv::Point> > contours_poly;
        std::vector<cv::Rect> boundRect;
        std::vector<cv::RotatedRect> outerEllipseRects;
        std::vector<cv::RotatedRect> innerEllipseRects;
        std::vector<int> areaIdx;

        findContours(threshIm, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
        pcl::PointCloud<pcl::PointXYZ>::Ptr contourPtCloud (new pcl::PointCloud<pcl::PointXYZ> ());
        int idx = 0;
        for (size_t i = 0; i < contours.size(); i++) {

            double area = contourArea(contours[i]);
            if (area > 45 && area < 600  && contours[i].size() > 4) {     //to fit ellipse you need 6 points
                double subArea;

                cv::RotatedRect innerRotRect;
                subArea = areaSumOfAllChildrenOf(contours, hierarchy, i, &innerRotRect);
                contourPtCloud = pointsAtIndex(contours[i], ptCloud);
                pcl::SampleConsensusModelCircle3D<pcl::PointXYZ>::Ptr
                        modelCircle3D(new pcl::SampleConsensusModelCircle3D<pcl::PointXYZ> (contourPtCloud));

                pcl::RandomSampleConsensus<pcl::PointXYZ> ransac (modelCircle3D);
                ransac.setDistanceThreshold (.01);
                ransac.computeModel();
                float circle_radius = ransac.model_coefficients_[3];

                // Detect sphere
//                std::float_t distance_threshold = 0.01;
//                std::vector<int> inliers;
//
//                pcl::SampleConsensusModelSphere<pcl::PointXYZ>::Ptr model_s(new pcl::SampleConsensusModelSphere<pcl::PointXYZ> (contourPtCloud));
//                model_s->setRadiusLimits(0.10, 0.20);
//                pcl::RandomSampleConsensus<pcl::PointXYZ> ransac2 (model_s);
//                ransac2.setDistanceThreshold (distance_threshold);
//                ransac2.computeModel();
//                ransac2.getInliers(inliers);
//
//                Eigen::VectorXf model_coefficients;
//                ransac2.getModelCoefficients(model_coefficients);
//                landMarkCoordinates.push_back(pcl::PointXYZ(model_coefficients[0], model_coefficients[1], model_coefficients[2]));

                contourPtCloud->clear();


                if (subArea > 5 && circle_radius >0.07 && circle_radius <0.08) {
                    cv::RotatedRect rotRect = fitEllipse(contours[i]);
//                    auto H = OS1::pixels_per_column;
//                    auto W = OS1::n_cols_of_lidar_mode(OS1::lidar_mode::MODE_1024x10);
//                    const auto px_offset = ouster::OS1::get_px_offset(W);
                    const size_t vv = (int)(rotRect.center.x + px_offset[round(rotRect.center.y)]) % W;
                    const size_t index = vv * H + rotRect.center.y;

                    landMarkCoordinates.push_back(pcl::PointXYZ(ptCloud.points[index].x, ptCloud.points[index].y, ptCloud.points[index].z));
                    idx = idx + 1;
                }
            }
        }
    }

    return landMarkCoordinates;
}


bool isDefinedPoint(pcl::PointXYZ point) {
    if (point.x == 0 && point.y == 0 && point.z == 0) {
        return false;
    }
    return true;
}
void imageCallback(const sensor_msgs::ImageConstPtr& msg)
 {
    ROS_DEBUG("Calling the image callback");
    try
    {
      cv::imshow("view", cv_bridge::toCvShare(msg, "bgr8")->image);
      cv::waitKey(30);
    }
    catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("Could not convert from '%s' to 'bgr8'.", msg->encoding.c_str());
    }
 }

int main(int argc, char** argv) {
    ros::init(argc, argv, "landmark_node");
    ros::NodeHandle nh("~");

    boost::uuids::random_generator generator;

    image_transport::ImageTransport it(nh);

    ouster_ros::OSConfigSrv cfg{};
    auto client = nh.serviceClient<ouster_ros::OSConfigSrv>("os_config");
    client.waitForExistence();

    if (!client.call(cfg)) {
        ROS_ERROR("Calling os config service failed");
        return EXIT_FAILURE;
    }

    extern sensor::sensor_info info;
    info = sensor::parse_metadata(cfg.response.metadata);

    uint32_t H = info.format.pixels_per_column;
    uint32_t W = info.format.columns_per_frame;

    const auto px_offset = info.format.pixel_shift_by_row;

    ros::Publisher landmarkListPublisher =
            nh.advertise<cartographer_ros_msgs::LandmarkList>("landmark", 100);

    Cloud cloud{};

    auto cloud_handler = [&](const sensor_msgs::PointCloud2::ConstPtr& m) {
        pcl::fromROSMsg(*m, cloud);

        cartographer_ros_msgs::LandmarkList landmarks;
        landmarks.header = m->header;
        landmarks.header.frame_id = "os_lidar";
        std::vector<cartographer_ros_msgs::LandmarkEntry>  landmarkEntries;


        cv::Mat intensity_image = cv::Mat(H, W, CV_32F);
        cv::Mat index_image = cv::Mat(H, W, CV_32SC1);

        for (uint32_t u = 0; u < H; u++) {
            for (uint32_t v = 0; v < W; v++) {
                const size_t vv = (v + px_offset[u]) % W;
                const size_t index = vv * H + u;
                const auto& pt = cloud[index];
                index_image.at<int>(u, v) = (int) index;
                intensity_image.at<float>(u, v) = pt.intensity;

            }
        }

        std::vector<pcl::PointXYZ> landMarkCoords;
        landMarkCoords = ptCloudIndicesOfLandMarks(intensity_image,0.5271f,cloud);

        for (size_t j = 0; j < landMarkCoords.size(); ++j) {
            pcl::PointXYZ landmarkPoint;
            landmarkPoint = landMarkCoords[j];
            if (isDefinedPoint(landmarkPoint)) {
                cartographer_ros_msgs::LandmarkEntry landmarkEntry;
                geometry_msgs::Pose pose;
                pose.position.x = -landmarkPoint.x;
                pose.position.y = -landmarkPoint.y;
                pose.position.z = -landmarkPoint.z;
                geometry_msgs::Quaternion quaternion;
                quaternion.x =0;
                quaternion.y =0;
                quaternion.z =0;
                quaternion.w =1;
                pose.orientation = quaternion;
                landmarkEntry.tracking_from_landmark_transform = pose;


                boost::uuids::uuid uuid = generator();
                landmarkEntry.id = boost::uuids::to_string(uuid);
                landmarkEntry.rotation_weight = 0;
                landmarkEntry.translation_weight = 0;

                landmarkEntries.push_back(landmarkEntry);
            }
        }




        landmarks.landmarks = landmarkEntries;
        landmarkListPublisher.publish(landmarks);
    };

    auto pc_sub =
            nh.subscribe<sensor_msgs::PointCloud2>("points", 100, cloud_handler);
    ROS_DEBUG("Creating window");
//    cv::namedWindow("view");
//    image_transport::Subscriber sub = it.subscribe("intensity_image", 1, imageCallback);

    ros::spin();
//    cv::destroyWindow("view");
    return EXIT_SUCCESS;
}